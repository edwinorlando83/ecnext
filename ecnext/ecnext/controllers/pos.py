from __future__ import unicode_literals
import frappe
 
def validate_POS_Closing_Entry(doc, method):
	if doc.docstatus ==1:
		verificar_monto_de_cierre(doc)
		verifica_existe_caja(doc)
		


def verifica_existe_caja(doc):
	totalExisente = verificar_cajachica( doc.period_start_date , doc.period_end_date )
	totalAsigando = len(doc.pos_caja_menor)
	if  totalExisente > totalAsigando :
		frappe.throw("No se ha registrado las salidas de Caja Chica")


def verificar_monto_de_cierre(doc):
	suma=0
	for row in doc.payment_reconciliation:
		suma+= row.closing_amount	
	if suma == 0:
		frappe.throw("El monto de Cierre no puede ser 0")

@frappe.whitelist()
def verificar_cajachica(fecha_inicio, fecha_fin):
	sql = "select count(*) contar from tabcaja_menor  where creation  BETWEEN   '{0}' and '{1}'   and pos_closing_entry is null".format(fecha_inicio,fecha_fin)
	obj = frappe.db.sql(sql)
	return obj[0][0]

@frappe.whitelist()
def get_cajachica(fecha_inicio, fecha_fin):
	sql = "select *  from tabcaja_menor  where creation  BETWEEN   '{0}' and '{1}'   and pos_closing_entry is null".format(fecha_inicio,fecha_fin)
	obj = frappe.db.sql(sql, as_dict = True)
	return obj 
