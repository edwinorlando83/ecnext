function registarPago(dato) {
    frappe.confirm('¿Estas seguro que deseas continuar? <hr> <strong>      El sistema va registrar como pagado. </strong>',
        () => {

            frappe.call({
                freeze: 'Aprobando ODA...',
                method: "ecnext.ecnext.report.reporte_ceditos.reporte_ceditos.registrarcomopagado",
                args: {
                    name_pos: dato
                },
                callback: (r) => {
                  frappe.show_alert(r.message)

                  frappe.show_alert({
                    message: __(r.message),
                    indicator: 'green'
                  }, 5);
                  frappe.query_report.refresh();
                 
                },
            })




        })
}