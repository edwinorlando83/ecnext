# Copyright (c) 2021, ORLANDO CHOLOTA and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
import shutil
class Configuraciones(Document):
	@frappe.whitelist()
	def btnpos(self):
		origen  = '/home/frappe/frappe-bench/apps/ecnext/ecnext/traducciones/point-of-sale.min.js'
		destino= '/home/frappe/frappe-bench/sites/assets/js/point-of-sale.min.js'
		shutil.copy2(origen, destino ) 
		return "Listo.. "

