// Copyright (c) 2021, ORLANDO CHOLOTA and contributors
// For license information, please see license.txt

frappe.ui.form.on('Configuraciones', {
    // refresh: function(frm) {

    // }
    btnpos: function(frm) {

        frappe.call({
            doc: cur_frm.doc,
            method: "btnpos",
            freeze: true,
            callback: (r) => {
                frappe.msgprint(
                    r.message
                );
            },
        });
    }
});