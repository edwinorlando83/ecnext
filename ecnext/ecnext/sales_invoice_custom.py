from __future__ import unicode_literals
import frappe
import ecnext.ecnext.ecutils as ec
import random

@frappe.whitelist()
def generarXmlFactura_NC(clave_acceso):
    msg= ec.genenarXmlFacturas(clave_acceso)
    frappe.throw(msg)

def generarNumeroFac(doc,method):
    if not doc.is_return:
        doc.naming_series ="edwin orlando2"

def getNumeroFac(doc,method):
    random.seed()
    sri_establecimiento=''
    obj_establecimiento = frappe.db.sql("""SELECT establecimiento,name fROM  `tabWarehouse`  where name ='{0}' and company = '{1}' """.format( doc.almacen, doc.company))

    sri_establecimiento= obj_establecimiento[0][0]


    if not sri_establecimiento:
        frappe.throw("EL establecimiento no se ha establecido en el almacen")



    obj_ambiente = frappe.db.sql("""select ambiente from tabEmisor where compania = %s""",doc.company)

    if not obj_ambiente:
        frappe.throw("EL Ambiente no se ha establecido en el Emisor")

    sri_ambiente = obj_ambiente[0][0]

    obj_ruc = frappe.db.sql("""select ruc from tabEmisor where compania = %s""",doc.company)

    if not obj_ruc:
        frappe.throw("EL ruc no se ha establecido en el Emisor")
    sri_ruc = obj_ruc[0][0]

    if str( sri_ambiente ).strip() == 'PRUEBAS':
        sri_ambiente = "1"
    else:
        sri_ambiente = "2"


    if not doc.sri_p_emision:
        frappe.throw("EL punto de Emision  no se ha espeificado")

    documento_inicial=''
    if doc.is_return == 0:
        tipoComprobante='01'
        documento_inicial='FAC-'
        if sri_ambiente == "1":
            documento_inicial='FACP-'
    else:
        tipoComprobante='04'
        documento_inicial='NC-'
        if sri_ambiente == "1":
            documento_inicial='NCP-'

    sqlnumfac="SELECT  case when max( sri_secuencial) is null then 1 else  max( sri_secuencial)+ 1 end as numero FROM `tabSales Invoice`    where   is_return = {0}    and sri_ambiente = '{1}'   and sri_establecimiento= '{2}'   and sri_p_emision= '{2}' ".format(doc.is_return,sri_ambiente, sri_establecimiento,doc.sri_p_emision)

    objnumerofac =  frappe.db.sql(sqlnumfac)
    numerofac = str (int(objnumerofac[0][0]))

    doc.sri_secuencial= numerofac
    numerofac = numerofac.zfill(9)
    numfac_completo = sri_establecimiento +doc.sri_p_emision+'-'+numerofac
    serie = sri_establecimiento + doc.sri_p_emision
    doc.numero_fac =documento_inicial+numfac_completo
    doc.sri_establecimiento = sri_establecimiento
    doc.sri_ambiente = sri_ambiente


    claveacceso = ec.generarClaveAcesso( doc.posting_date,tipoComprobante,sri_ruc,sri_ambiente,serie,numerofac)
    doc.sri_claveacceso=claveacceso
    #ruta = os.path.dirname(__file__)
    #ruta = frappe.local.site
    # ruta = os.path.abspath(frappe.get_site_path("public", "files"))
    #frappe.throw(ruta)
	
@frappe.whitelist()	
def getPuntoEmision(almacen=None):
    puntosemi = " "
    if almacen:
        puntosemi =  frappe.db.sql_list(""" SELECT codigo FROM  tabPuntoEmision where parent=%s order by codigo """,almacen)
    return puntosemi

@frappe.whitelist()	
def getPuntoEmisionGuardado(numero_fac=None,almacen=None):

    puntosemis =  frappe.db.sql_list(""" SELECT codigo FROM  tabPuntoEmision where parent=%s order by codigo """,almacen)
    puntosemi =  frappe.db.sql_list(""" select sri_establecimiento from `tabSales Invoice`  where numero_fac = %s """,numero_fac)
    lista=puntosemis+puntosemi
    return lista






