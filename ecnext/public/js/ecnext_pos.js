frappe.ui.form.on('POS Closing Entry', {
	onload: function (frm) {

	},
	refresh: function (frm) {


	},
	validate: function (frm) {

	},
	onsubmit: function (frm) {
	},
	verificar_salidas: function (frm) {
		frappe.call({
			method: "ecnext.ecnext.controllers.pos.get_cajachica",
			args: {
				fecha_inicio: frm.doc.period_start_date, fecha_fin: frm.doc.period_end_date
			},
			freeze: true,
			callback: (r) => {
				console.log(r.message);
				frm.clear_table("pos_caja_menor");
				let suma=0;
				r.message.forEach(function (e) {
					frm.add_child('pos_caja_menor', { caja_menor: e.name , fecha: e.creation , detalle:e.detalle,valor:e.valor});
				 suma += e.valor;
				});
				frm.doc.total_caja_chica = suma;
				frm.refresh_field('total_caja_chica');
				frm.refresh_field('pos_caja_menor');
			},
		});
	}

});

