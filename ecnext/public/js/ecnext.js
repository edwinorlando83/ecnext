frappe.ui.form.on('Sales Invoice', {
	onload: function(frm) {  
	
	frm.page.add_menu_item(__("Generar XML"), function() {		
	 	 frappe.call({
         method: 'ecnext.ecnext.sales_invoice_custom.generarXmlFactura_NC', 
		 args:{
   			clave_acceso:frm.get_field('sri_claveacceso').value
   			},				 
         callback: (r) => {
             
	    	 
         },
      });  	 
	  
		});
	},	
	refresh: function(frm) {
      	if (frm.doc.numero_fac != undefined){			 
			   getPuntosEmisionGuardado( frm,frm.doc.numero_fac,frm.doc.almacen); 
		}  
	},
	validate: function(frm){
	 
	},	
	onsubmit: function(frm){
	
	},
    onload_post_render: function(frm){
	  
	},
	
	'almacen':function(frm){		
		getPuntosEmision(frm);								
	} ,
	
	'update_stock':function(frm){		
	//	alert(	frm.doc.update_stock); 						
	}  
	
	
	
});



function getPuntosEmision(frm)
{
	frappe.call({
         method: 'ecnext.ecnext.sales_invoice_custom.getPuntoEmision', 
		 args:{
   			almacen:frm.get_field('almacen').value
   			},				 
         callback: (r) => {
              // frappe.show_alert('hkg' );
	     frm.set_df_property('sri_p_emision', 'options', r.message);
		 frm.refresh_field('sri_p_emision');	
   	     frm.set_value("sri_p_emision", r.message[0]);		 
         },
      });  	
	
}
function getPuntosEmisionGuardado(frm,innumero_fac,inalmacen)
{
	frappe.call({
         method: 'ecnext.ecnext.sales_invoice_custom.getPuntoEmisionGuardado', 
		 args:{
   			numero_fac:innumero_fac,
			almacen:inalmacen
   			},				 
         callback: (r) => {
			console.log(  r.message);
			 var peGuardado = r.message[0];
			 var lisata=  r.message.slice(1)
			
              frappe.show_alert( r.message[0]);
	      frm.set_df_property('sri_p_emision', 'options', lisata);
		  frm.refresh_field('sri_p_emision');	
   	     frm.set_value("sri_p_emision", peGuardado);		 
         },
      });  	
	
}


frappe.ui.form.on('Customer', {
	onload: function(frm) {  	
		console.log("Customer-----onload");
		 
	},	
	refresh: function(frm) {       
	},
	validate: function(frm){	
		console.log("Customer----validate");
		validar();	 
	},	
	onsubmit: function(frm){	
	},
	'dni':function(frm){
	  validar();
		   
	} 	
});

function validar()
{
	var res = validarCedula(frm.doc.dni) ; 
	if( res===0)
		frappe.throw(__("El # de cedula es incorrecto:" + frm.doc.cedula));	 
}
function validarCedula(cad) { 	 
	var total = 0;
	var longitud = cad.length;
	var longcheck = longitud - 1;

	if (cad !== '' && longitud === 10){
	
	  for(var i = 0; i < longcheck; i++){
		  
		if (i%2 === 0) {
		  var aux = cad.charAt(i) * 2;
		  if (aux > 9) aux -= 9;
		  total += aux;
		} else {
		  total += parseInt(cad.charAt(i));
		}
		
	  }
	  total = total % 10 ? 10 - total % 10 : 0;
	  if (cad.charAt(longitud-1) == total) {
		
		return 1;
	  }else{
			 
		return 0;
	  }
	}  
	else 		
	return 0;
 
  }
